ics-ans-role-conda
==================

Ansible role to install conda on CentOS.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
conda_version: 4.4.11
miniconda_version: Miniconda3-4.3.31
miniconda_installer: "{{miniconda_version}}-Linux-x86_64.sh"
miniconda_installer_md5: 7fe70b214bee1143e3e3f0467b71453c
# List of conda environment yaml files to create
conda_env_files: []
```

You can force the conda user id by setting the `conda_uid` variable (not defined by default,
uid automatically generated).

The role can create conda environments if you pass a list of yaml environment files via
the `conda_env_files` variable.
You could use something like the following in your playbook:

```yaml
conda_env_files:
  - "{{ playbook_dir }}/config/molecule_env.yml"
```


Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conda
      miniconda_version: Miniconda2-4.3.11
      miniconda_installer_md5: d573980fe3b5cdf80485add2466463f5
```

License
-------

BSD 2-clause
