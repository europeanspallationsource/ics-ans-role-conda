---
- name: check if conda is installed
  shell: >
    /opt/conda/bin/conda --version 2>&1 | grep -qw {{conda_version}}
  check_mode: no
  failed_when: False
  changed_when: False
  register: is_conda_installed

- name: is conda installed
  debug:
    msg: "conda {{conda_version}} is installed: {{is_conda_installed.rc == 0}}"

- name: install required packages
  yum:
    name: "{{item}}"
    state: present
  with_items:
    - bzip2
    - sudo

- name: create conda user
  user:
    name: conda
    comment: "conda user"
    uid: "{{conda_uid | default(omit)}}"
    group: users
    shell: /bin/bash
    home: /home/conda
    system: yes

- name: delete previous conda installation
  file:
    path: /opt/conda
    state: absent
  when: is_conda_installed.rc != 0

- name: create conda directory
  file:
    path: /opt/conda
    state: directory
    mode: 0755
    owner: conda
    group: users

- name: download miniconda installer
  get_url:
    url: https://repo.continuum.io/miniconda/{{miniconda_installer}}
    checksum: md5:{{miniconda_installer_md5}}
    dest: /opt/conda
    owner: conda
    group: users
    mode: 0644
  when: is_conda_installed.rc != 0

- name: install conda
  command: >
    bash /opt/conda/{{miniconda_installer}} -p /opt/conda -b -f
  become: yes
  become_user: conda
  when: is_conda_installed.rc != 0

- name: install specific conda version
  command: >
    /opt/conda/bin/conda install -y conda={{conda_version}}
  become: yes
  become_user: conda
  when: is_conda_installed.rc != 0

- name: delete miniconda installer
  file:
    path: /opt/conda/{{miniconda_installer}}
    state: absent

- name: delete old conda_env.sh file
  file:
    path: /etc/profile.d/conda_env.sh
    state: absent

- name: create link to conda.sh profile
  file:
    src: /opt/conda/etc/profile.d/conda.sh
    dest: /etc/profile.d/conda.sh
    state: link

- name: copy condarc files
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/.condarc"
    owner: conda
    group: users
    mode: 0644
  with_items:
    - src: user_condarc
      dest: /home/conda
    - src: system_condarc
      dest: /opt/conda

# See https://github.com/conda/conda/issues/6576
- name: create empty environments.txt due to conda bug
  file:
    path: /home/conda/.conda/environments.txt
    owner: conda
    group: users
    mode: 0644
    state: touch
  changed_when: False

- name: create conda environment(s)
  include_tasks: create_conda_env.yml
  with_items: "{{ conda_env_files }}"
  loop_control:
    loop_var: env_file
