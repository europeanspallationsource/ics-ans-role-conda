# Tests for the set_uid group
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('set_uid')


def test_conda_uid(host):
    user = host.user('conda')
    assert user.uid == 48


def test_conda_no_env_created(host):
    cmd = host.run('ls /opt/conda/envs')
    assert cmd.rc == 0
    assert cmd.stdout.strip() == ''
