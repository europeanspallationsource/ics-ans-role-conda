# Tests for the create_env group
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('create_env')


def test_conda_env_python27(host):
    assert host.file('/opt/conda/envs/python27/bin').exists
    cmd = host.run('/opt/conda/envs/python27/bin/python --version 2>&1')
    assert cmd.stdout.startswith('Python 2.7')


def test_conda_env_python36(host):
    assert host.file('/opt/conda/envs/python36/bin').exists
    cmd = host.run('/opt/conda/envs/python36/bin/python --version 2>&1')
    assert cmd.stdout.startswith('Python 3.6')
