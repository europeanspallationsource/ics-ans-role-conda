import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_conda_user(host):
    user = host.user('conda')
    assert user.exists
    assert user.group == 'users'
    assert user.home == '/home/conda'
    assert user.shell == '/bin/bash'
    assert user.expiration_date is None


def test_conda_version(host):
    cmd = host.run('/opt/conda/bin/conda --version 2>&1')
    assert cmd.rc == 0
    assert cmd.stdout.strip() == 'conda 4.4.11'


def test_conda_path(host):
    cmd = host.run('su -l -c "type conda" conda')
    assert cmd.stdout.startswith('conda is a function')
